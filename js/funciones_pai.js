function modificarPai(){
	
	$form=$('#formulario-pai');
	let modoBusqueda = $('#ver-modificar-plan').find('.modoBusquedaResidente').val();
	let idResidente = busquedaIdResidente(modoBusqueda,$('#ver-modificar-plan'));
	let respuesta;
	let data;
	let datosSociosanitarios=new Object();
	let areaSocial=new Object();
	let areaSanitaria=new Object();
	let areaPsicologica=new Object();
	let areaFuncional = new Object();
	let areaAnimacion = new Object();
	let dolor = new Object;
	let fechaElaboracion= $form.find('[name="fecha-evaluacion-pai"]').val();
	let fechaConvertida = convertirFechaEspIngles(fechaElaboracion.substr(0,10))+(fechaElaboracion.substr(10));	
	let $datosSociosanitariosFormControl = $(".datosSociosanitarios .form-control.anadir");
	let $areaSocialFormControl = $(".areaSocial .form-control");
	let $areaSanitariaFormControl = $(".areaSanitaria .form-control");
	let $areaPsicologicaFormControl = $(".areaPsicologica .form-control");
	let $areaFuncionalFormControl = $(".areaFuncional .form-control");
	let $areaAnimacionFormControl = $(".areaAnimacion .form-control");
	
	$datosSociosanitariosFormControl.each(function(index){
		datosSociosanitarios[$(this).attr('name')] = $(this).val();
	});
	
	$areaSocialFormControl.each(function(index){
		areaSocial[$(this).attr('name')] = $(this).val();
	});
	
	$areaSanitariaFormControl.each(function(index){
		if($(this).hasClass("summernote"))
			areaSanitaria[$(this).attr('name')] = $(this).summernote('code');
		else if($(this).hasClass("dolor"))
			dolor[$(this).attr('name')] = $(this).val();
		else
			areaSanitaria[$(this).attr('name')] = $(this).val();
	});
	
	$areaPsicologicaFormControl.each(function(index){
		areaPsicologica[$(this).attr('name')] = $(this).val();
	});
	
	$areaFuncionalFormControl.each(function(index){
		areaFuncional[$(this).attr('name')] = $(this).val();
	});
	
	$areaAnimacionFormControl.each(function(index){
		areaAnimacion[$(this).attr('name')] = $(this).val();
	});
	
	var $creenciasCheckbox = $("[class~='creenciasCheckbox'] input[type=checkbox]");
	var $areaSanitariaCheckbox = $("[class~='areaSanitariaCheckbox'] input[type=checkbox]");
	var $dolorCheckbox = $("[class~='dolorCheckbox'] input[type=checkbox]");
	var $areaPsicologicaCheckbox = $("[class~='areaPsicologicaCheckbox'] input[type=checkbox]");
	var $areaFuncionalCheckbox = $("[class~='areaFuncionalCheckbox'] input[type=checkbox]");
	var $areaAnimacionCheckbox = $("[class~='areaAnimacionCheckbox'] input[type=checkbox]");
	

	$creenciasCheckbox.each(function(index, check ){
		areaSocial[check.id] = check.checked;		
	});
	
	$areaSanitariaCheckbox.each(function(index, check ){
		areaSanitaria[check.id] = check.checked;		
	});
	
	$dolorCheckbox.each(function(index, check ){
		dolor[check.id] = check.checked;		
	});
	
	$areaPsicologicaCheckbox.each(function(index, check ){
		areaPsicologica[check.id] = check.checked;		
	});
	
	$areaFuncionalCheckbox.each(function(index, check ){
		areaFuncional[check.id] = check.checked;		
	});
	
	$areaAnimacionCheckbox.each(function(index, check ){
		areaAnimacion[check.id] = check.checked;		
	});
	
	areaSanitaria['dolor']=dolor;
	
	var datosResidente={
		pai_datos_sociosanitarios : JSON.stringify(datosSociosanitarios,escaparSaltosLinea),
		pai_area_social : JSON.stringify(areaSocial,escaparSaltosLinea),
		pai_area_sanitaria : JSON.stringify(areaSanitaria,escaparSaltosLinea),
		pai_area_psicologica : JSON.stringify(areaPsicologica,escaparSaltosLinea),
		pai_area_funcional : JSON.stringify(areaFuncional,escaparSaltosLinea),
		pai_area_animacion : JSON.stringify(areaAnimacion,escaparSaltosLinea),
		grado_dependencia : $form.find('[name="grado_dependencia"]').val(),
		incapacidad_legal : $form.find('[name="incapacidad_legal"]').val(),
		pai_fecha_elaboracion : fechaConvertida
	};
	
	var objetoCamposWhere = {			
		id_residente : idResidente		
	}

	if($("#otroFamiliar").prop('checked')){
		var datosOtroFamiliar={
			dni_familiar : $form.find('[name="dni-otros-pai"]').val(),
			nombre_familiar : $form.find('[name="nombre-otros-pai"]').val(),
			apellidos : $form.find('[name="apellidos-otros-pai"]').val(),
			telefono :  $form.find('[name="telefono-otros-pai"]').val(),
			parentesco : $form.find('[name="parentesco-otros-pai"]').val()				
		};
		
		data = {datosResidente,datosOtroFamiliar,objetoCamposWhere};
	
	}else{
		data = {datosResidente,objetoCamposWhere};
	}
			
	
	
	$.ajax({
		url: '../controllers/residente.php?task=modificarPai',
		type: "POST",
		async: false,
		data: data,
		dataType: "html",
		success: function(data) {
			let respuesta =  $.parseJSON(data);
			if(respuesta.status == "WARNING"){
				swal({
					title: "MODIFICADO CON WARNING",
					text:  respuesta.message,
					buttons: false,
					icon:  "warning",
					timer: 2500,
				});
				
			}else{
				swal({
					title: "MODIFICADO",
					text:  "Modificado correctamente",
					buttons: false,
					icon:  "success",
					timer: 1500,
				});
			}
			setTimeout('location.reload()',2000); 
		},
		error: function(data) {
			let respuesta =  $.parseJSON(data.responseText);
			swal({
				title: "ERROR",
				text:  respuesta.message,
				buttons: false,
				icon:  "error",
				timer: 2500,
			});
		}
	});
	
}


function cargarDatosPai(){
	
	let modoBusqueda = $('#ver-modificar-plan').find('.modoBusquedaResidente').val();
	let idResidente = busquedaIdResidente(modoBusqueda,$('#ver-modificar-plan'));
	let respuesta;
	let datosResidente;
	let datosOtroFamiliar;
	$form=$('#formulario-pai');
		
	var postObj = {			
		id_residente : idResidente
	}

    $.ajax({
        url: '../controllers/residente.php?task=consultarPai',
        type: "POST",
        async: false,
        data: postObj,
		dataType: "html",
        success: function(data) {	
			respuesta = $.parseJSON(data);
			datosResidente =  respuesta.datosResidente;
			if(respuesta.datosOtroFamiliar!=null)
				datosOtroFamiliar =  respuesta.datosOtroFamiliar;
			if(respuesta.status == "WARNING"){
				swal({
					title: "WARNING",
					text:  respuesta.message,
					buttons: false,
					icon:  "warning",
					timer: 2500,
				});
			}
		}
	});
	if(datosResidente != undefined && Object.keys(datosResidente).length > 0){
		rellenarPai(datosResidente);
		if(datosOtroFamiliar != undefined && Object.keys(datosOtroFamiliar).length > 0){
			$("#otroFamiliar").prop('checked', true);
			$('.datosOtroFamiliar').prop('disabled', false);
			$form.find('[name="nombre-otros-pai').val(datosOtroFamiliar.nombre_familiar);
			$form.find('[name="apellidos-otros-pai').val(datosOtroFamiliar.apellidos);
			$form.find('[name="dni-otros-pai').val(datosOtroFamiliar.dni_familiar);
			$form.find('[name="telefono-otros-pai').val(datosOtroFamiliar.telefono);
			$form.find('[name="parentesco-otros-pai').val(datosOtroFamiliar.parentesco);
		}else{
			$("#otroFamiliar").prop('checked', false);
			$('.datosOtroFamiliar').prop('disabled', true);			
		}
		
	}else{
		$('#seccionPai').find($('.form-control')).each(function(index){
			$(this).val("");
		});		
	}
}

function rellenarPai(datosResidente){
	
	$form=$('#formulario-pai');
	let fechaActual = new Date();
	let fechaIngreso = new Date(datosResidente.fecha_alta);
	
	$form.find('[name="fecha-alta-pai').val(fechaIngreso.toLocaleString('en-GB'));
	$form.find('[name="fecha-evaluacion-pai').val(fechaActual.toLocaleString('en-GB'));
	
	if(datosResidente.pai_fecha_elaboracion!=null){
		let fechaElaboracion = new Date(datosResidente.pai_fecha_elaboracion);
		$form.find('[name="fecha-elaboracion-pai').val(fechaElaboracion.toLocaleString('en-GB'));
	}
	
	$form.find('[name="nombre-familiar-pai"]').val(datosResidente.apellidos + ", " + datosResidente.nombre_familiar);
	$form.find('[name="nombre-residente-pai"]').val(datosResidente.apellido1 + " " + datosResidente.apellido2 + ", " + datosResidente.nombre);

	//recuperamos los datos del residente, familiar, otro familiar...
	Object.entries(datosResidente).forEach(([clave, valor]) => {
		let name= "[name='" + clave +"']";
		$form.find(name).val(valor);
	});
	
	//añadimos el responable, si lo tuviera	
	if(datosResidente.id_habitacion != null && datosResidente.id_habitacion != "" && datosResidente.id_personal_responsable != null &&  datosResidente.id_personal_responsable != ""){
		$form.find('[name="responasable-pai"]').empty();
		mostrarResponsablesPlanta(datosResidente.id_habitacion);
		$form.find('[name="responasable-pai"] option').each(function(){
			if ($(this).val() != datosResidente.id_personal_responsable )        
				$(this).remove();	    
		});
	}else{
		$form.find('[name="responasable-pai"]').empty();
		$form.find('[name="responasable-pai"]').append($('<option>').text("No tiene responsable asignado").attr('value', ""));
	}

	//Recuperamos los diferentes JSON de la base de datos (de las diferentes áreas)
	let respuestaDatosSociosanitarios= $.parseJSON(datosResidente.pai_datos_sociosanitarios);
	let respuestaAreaSocial= $.parseJSON(datosResidente.pai_area_social);
	let respuestaAreaSanitaria= $.parseJSON(datosResidente.pai_area_sanitaria);	
	let respuestaAreaPsicologica= $.parseJSON(datosResidente.pai_area_psicologica);
	let respuestaAreaFuncional= $.parseJSON(datosResidente.pai_area_funcional);
	let respuestaAreaAnimacion= $.parseJSON(datosResidente.pai_area_animacion);
	
	//por otro lado, obtenemos los campos del formulario (por áreas) 
	let $datosSociosanitariosFormControl = $(".datosSociosanitarios .form-control.anadir");
	let $areaSocialFormControl = $(".areaSocial .form-control");
	let $creenciasCheckbox = $("[class~='creenciasCheckbox'] input[type=checkbox]");
	
	let $areaSanitariaFormControl = $(".areaSanitaria .form-control");
	let $areaSanitariaCheckbox = $("[class~='areaSanitariaCheckbox'] input[type=checkbox]");
	let $dolorCheckbox = $("[class~='dolorCheckbox'] input[type=checkbox]");
	
	let $areaPsicologicaFormControl = $(".areaPsicologica .form-control");
	let $areaPsicologicaCheckbox = $("[class~='areaPsicologicaCheckbox'] input[type=checkbox]");
	
	let $areaFuncionalFormControl = $(".areaFuncional .form-control");
	let $areaFuncionalCheckbox = $("[class~='areaFuncionalCheckbox'] input[type=checkbox]");
	
	let $areaAnimacionFormControl = $(".areaAnimacion .form-control");
	let $areaAnimacionCheckbox = $("[class~='areaAnimacionCheckbox'] input[type=checkbox]");
	
	//Si ya hay datos en la BD, asignamos estos valores a los campos del área SOCIOSANITARIA
	if(respuestaDatosSociosanitarios != undefined && Object.keys(respuestaDatosSociosanitarios).length > 0){
		$datosSociosanitariosFormControl.each(function(index){
			if(respuestaDatosSociosanitarios[$( this ).attr('name')] != undefined)
				$(this).val(respuestaDatosSociosanitarios[$( this ).attr('name')]);
		});
		
	}
	
	//Si ya hay datos en la BD, asignamos estos valores a los campos del área SOCIAL
	if(respuestaAreaSocial != undefined && Object.keys(respuestaAreaSocial).length > 0){
		$areaSocialFormControl.each(function(index){
			if(respuestaAreaSocial[$( this ).attr('name')] != undefined)
				$(this).val(respuestaAreaSocial[$( this ).attr('name')]);
		});
		
		//Hacemos lo propio con los checkboxes, y si están seleccionados aquellos que tienen campos ocultos, los mostramos
		$creenciasCheckbox.each(function(index, check ){		
			$(check).prop('checked',respuestaAreaSocial[check.id]);
			if(check.id == "acudeServicios"){
				if(respuestaAreaSocial[check.id]!=false){
					$("[name='frecuenciaServicio']").val(respuestaAreaSocial['frecuenciaServicio']);
					$("#frecuenciaServicioSelect").removeClass("d-none");
				}else{
					$("#frecuenciaServicioSelect").addClass('d-none');
					$("[name='frecuenciaServicio']").val("");
				}
			}
		});
	}
	
	//Si ya hay datos en la BD, asignamos estos valores a los campos del área SANITARIA
	if(respuestaAreaSanitaria != undefined && Object.keys(respuestaAreaSanitaria).length > 0){
		$areaSanitariaFormControl.each(function(index){
			if(respuestaAreaSanitaria[$(this).attr('name')] != undefined || respuestaAreaSanitaria['dolor'][$(this).attr('name')] != undefined){
				if($(this).hasClass("summernote"))
					$(this).summernote('code',respuestaAreaSanitaria[$(this).attr('name')]);
				else if($(this).hasClass("dolor"))
					$(this).val(respuestaAreaSanitaria['dolor'][$(this).attr('name')]);
				else
					$(this).val(respuestaAreaSanitaria[$(this).attr('name')]);
				
				
			}
		});
		
		//Hacemos lo propio con los checkboxes, y si están seleccionados aquellos que tienen campos ocultos, los mostramos
		$areaSanitariaCheckbox.each(function(index, check ){		
			$(check).prop('checked',respuestaAreaSanitaria[check.id]);
			if(check.id == "ulcerasPiel"){
				if(respuestaAreaSanitaria[check.id]!=false){
					$("#camposUlcera").removeClass("d-none");
				}else{
					$("#camposUlcera").addClass('d-none');
					$('#gradoUlceraVascular').val('');
					$('#tratamientosUlceraVascular').val('');
					$('#otrosDatosUlceraVascular').val('');
				}
			}
			if(check.id == "ayudasTécnicas"){
				if(respuestaAreaSanitaria[check.id]!=false)
					$("#camposAyudasTecnicas").removeClass("d-none");
				else
					$("#camposAyudasTecnicas").addClass('d-none');
			}
			if(check.id == "encamamiento"){
				if(respuestaAreaSanitaria[check.id]!=false)
					$("#camposEncamamiento").removeClass("d-none");
				else
					$("#camposEncamamiento").addClass('d-none');
			}
		});
		
		$dolorCheckbox.each(function(index, check ){		
			$(check).prop('checked',respuestaAreaSanitaria['dolor'][check.id]);
		});
		
	}
	
	//Si ya hay datos en la BD, asignamos estos valores a los campos del área Psicologica
	if(respuestaAreaPsicologica != undefined && Object.keys(respuestaAreaPsicologica).length > 0){
		$areaPsicologicaFormControl.each(function(index){
			if(respuestaAreaPsicologica[$( this ).attr('name')] != undefined)
				$(this).val(respuestaAreaPsicologica[$( this ).attr('name')]);
		});
		
		//Hacemos lo propio con los checkboxes, y si están seleccionados aquellos que tienen campos ocultos, los mostramos
		$areaPsicologicaCheckbox.each(function(index, check ){		
			$(check).prop('checked',respuestaAreaPsicologica[check.id]);
			if(check.id == "antecedentesPsiquiatricos"){
				if(respuestaAreaPsicologica[check.id]!=false)
					$("#antecedentesPsiquiatricosArea").removeClass("d-none");
				else
					$("#antecedentesPsiquiatricosArea").addClass('d-none');
			}
		});
	}
	
	
	//Si ya hay datos en la BD, asignamos estos valores a los campos del área Funcional
	if(respuestaAreaFuncional != undefined && Object.keys(respuestaAreaFuncional).length > 0){
		$areaFuncionalFormControl.each(function(index){
			if(respuestaAreaFuncional[$( this ).attr('name')] != undefined)
				$(this).val(respuestaAreaFuncional[$( this ).attr('name')]);
		});
		
		//Hacemos lo propio con los checkboxes, y si están seleccionados aquellos que tienen campos ocultos, los mostramos
		$areaFuncionalCheckbox.each(function(index, check ){		
			$(check).prop('checked',respuestaAreaFuncional[check.id]);
		});
	}
	
	
	//Si ya hay datos en la BD, asignamos estos valores a los campos del área Animacion Sociocultural
	if(respuestaAreaAnimacion != undefined && Object.keys(respuestaAreaAnimacion).length > 0){
		$areaAnimacionFormControl.each(function(index){
			if(respuestaAreaAnimacion[$( this ).attr('name')] != undefined)
				$(this).val(respuestaAreaAnimacion[$( this ).attr('name')]);
		});
		
		//Hacemos lo propio con los checkboxes, y si están seleccionados aquellos que tienen campos ocultos, los mostramos
		$areaAnimacionCheckbox.each(function(index, check ){		
			$(check).prop('checked',respuestaAreaAnimacion[check.id]);
		});
	}

	//por último, activamos los campos ocultos que tengan información
	let $camposOcultos = $form.find($(".d-none"));
		
		$camposOcultos.each(function(index){
			if($( this ).val()!="")
				$(this).removeClass("d-none");
			else 
				$(this).addClass("d-none");
		});
		
	//Lo mismo con los disabled
	let $camposDisabled = $form.find(":disabled");
		
		$camposDisabled.each(function(index){
			if($( this ).val()!="")
				$(this).prop('disabled', false);
			else 
				$(this).prop('disabled', true);
		});
		
	//rellenamos la tabla de tratamientos
	tablaTratamientos(datosResidente.id_residente);
	
}

function escaparSaltosLinea(key, value) {
	let resultado = value;
  if (typeof value === "string") {
	resultado = value.replace(/\n/g, "\\n").replace(/\"/g, '\\"');
  }
  return resultado;
}

function limpiarPai(){
	$form=$('#formulario-pai');
	$form.find($(".form-control")).each(function(index){
		$( this ).val("");
		if($(this).hasClass("summernote"))
			$(this).summernote('code',"");
		if($(this).hasClass("dolor") && $(this).hasClass("selectConOtras"))
			$(this).prop('disabled', true);
	});
	
	$form.find($("input[type=checkbox]")).each(function(index){
			$( this ).prop('checked',false);
	});
	
	$form.find($(".ocultable")).each(function(index){
		$(this).addClass("d-none");
	});
}




