
function altaResidente(){
	
	var datosDiscapacidad=new Object();
	var datosSitMedica=new Object();
	
	var $discapacidadCheckBoxes = $("[class~='discapacidadAlta'] input[type=checkbox]");
	var $sitMedicaCheckBoxes = $("[class~='sitMedicaAlta'] input[type=checkbox]");
	
	$discapacidadCheckBoxes.each(function(index, check ){
		datosDiscapacidad[check.id] = check.checked;
		if(check.id == "otraDiscapacidad" && check.checked)
			datosDiscapacidad['otraDiscapacidadTexto'] = $("[name='otraDiscapacidadTexto']").val();			
	});
	
	$sitMedicaCheckBoxes.each(function(index, check ){
		datosSitMedica[check.id] = check.checked;
		if(check.id == "otraSitMedica" && check.checked)
			datosSitMedica['otraSitMedicaTexto'] = $("[name='otraSitMedicaTexto']").val();			
	});
	
	if($(".habIndividual").is(':checked')) 
		id_habitacion = $('#habitacionIndivAsignada').val();
	else
		id_habitacion = $('#habitacionDobleAsignada').val();
	
	var nExpediente= obtenerNumeroExpediente($('#apellido1Residente').val(),$('#nombreResidente').val());
		
	var datosResidente ={

        dni_residente : $('#dniResidente').val(),
		n_expediente : nExpediente,
        nombre : $('#nombreResidente').val(),
        apellido1 : $('#apellido1Residente').val(),
        apellido2 : $('#apellido2Residente').val(),
        fecha_nacimiento : $('#fechaNacimiento').val(),
        grado_dependencia : $('#gradoDependencia').val(),
		discapacidad : JSON.stringify(datosDiscapacidad,escaparComillas),
		situacion_medica : JSON.stringify(datosSitMedica,escaparComillas),
		id_personal_responsable : $('#responsableResidente').val(),
		id_habitacion : id_habitacion
	};
	
	var datosFamiliar={
		
		dni_familiar : $('#dniFamiliar').val(),
        nombre_familiar : $('#nombreFamiliar').val(),
        apellidos : $('#apellidosFamiliar').val(),
        direccion_postal : $('#direccionFamiliar').val(),
		codigo_postal : $('#codigoPostalFamiliar').val(),
		parentesco : $('#parentescoFamiliar').val(),
        telefono : $('#telefonoFamiliar').val(),
        email : $('#emailFamiliar').val()
	};

    //TABLA RESIDENTE
    $.ajax({
        url: '../controllers/residente.php?task=altaResidente',
        type: "POST",
        data: {datosResidente,datosFamiliar},
		async: false,
        dataType: "html",
		success: function(data) {		
			let respuesta =  $.parseJSON(data);
			if(respuesta.status == "WARNING"){
				swal({
					title: "INSERTADO CON WARNING",
					text:  respuesta.message,
					buttons: false,
					icon:  "warning",
					timer: 2500,
				});
				
			}else{
				swal({
					title: "INSERTADO",
					text:  "Residente insertado correctamente",
					buttons: false,
					icon:  "success",
					timer: 1500,
				});
			}
			setTimeout('location.reload()',2000); 
		},
		error: function(data) {
			let respuesta =  $.parseJSON(data.responseText);
			swal({
				title: "ERROR",
				text:  respuesta.message,
				buttons: false,
				icon:  "error",
				timer: 2500,
			});
		}
    });	
}


function obtenerNumeroExpediente(apellido1,nombre){
	
	var nExpediente="";
	var prefijo= (apellido1.substring(0,2) + nombre.substring(0,1)).toUpperCase();
	
	var postObj = {			
		prefijo : prefijo
	}
	
	$.ajax({
        url: '../controllers/residente.php?task=getExpediente',
        type: "POST",
        async: false,
        data: postObj,
		dataType: "html",
        success: function(data) {		
			nExpediente =  $.parseJSON(data).nExpediente;	
		}
	});
	
	return nExpediente;

}

function bajaResidente(){
	
	let modoBusqueda = $('#ver-baja-residente').find('.modoBusquedaResidente').val();
	let idResidente = busquedaIdResidente(modoBusqueda,$('#ver-baja-residente'));
	let motivoBaja;
	let fechaEng= convertirFechaEspIngles($('#fechaBajaResidente').val());
	let resultadoBusqueda = $('#busquedaBajaResidente').val();
	
	motivoBaja = $('#motivoBajaResidente').val();
	
	if(motivoBaja == "otras")
		motivoBaja = $('#textoOtrasBajaResidente').val();
			
	var objetoCamposUpdate = {			
		fecha_baja : fechaEng,
		motivo_baja : motivoBaja
	}
	
	var objetoCamposWhere = {			
		id_residente : idResidente
	}
	           
    $.ajax({
		url: '../controllers/residente.php?task=bajaResidente',
		type: "POST",
		async: false,
		data: {objetoCamposUpdate,objetoCamposWhere},
		dataType: "html",
		success: function(data) {
			swal({
				title: "ACTUALIZADO",
				text:  "Residente dado de baja correctamente",
				buttons: false,
				icon:  "success",
				timer: 2500,
			});
			
			setTimeout('location.reload()',2000); 
		},
		error: function(data) {
			let respuesta =  $.parseJSON(data.responseText);
			swal({
				title: "ERROR",
				text:  respuesta.message,
				buttons: false,
				icon:  "error",
				timer: 2500,
			});
		}
	});
}

function cargarDatosResidente(){
	
	let modoBusqueda = $('#ver-modificar-residente').find('.modoBusquedaResidente').val();
	let idResidente = busquedaIdResidente(modoBusqueda,$('#ver-modificar-residente'));
	let respuesta;
	let datosResidente;
	let $form = $('#seccionModificarResidente');
	let $discapacidadCheckBoxes = $("[class~='discapacidadModif'] input[type=checkbox]");
	let $sitMedicaCheckBoxes = $("[class~='sitMedicaModif'] input[type=checkbox]");
	
	let postObj = {			
		id_residente : idResidente
	}

    $.ajax({
        url: '../controllers/residente.php?task=consultarResidente',
        type: "POST",
        async: false,
        data: postObj,
		dataType: "html",
        success: function(data) {	
			respuesta = $.parseJSON(data);
			datosResidente =  respuesta.datosResidente;
			if(respuesta.status == "WARNING"){
				swal({
					title: "WARNING",
					text:  respuesta.message,
					buttons: false,
					icon:  "warning",
					timer: 2500,
				});
			}
		}
	});
	if(datosResidente != undefined && Object.keys(datosResidente).length > 0){
		
		//recuperamos los datos del residente, familiar...
		Object.entries(datosResidente).forEach(([clave, valor]) => {
			let name= "[name='" + clave +"']";
			$form.find(name).val(valor);
		});

		//recuperamos los datos de la habitación y del responsable
		if(datosResidente.id_habitacion != null && datosResidente.id_habitacion != ""){
			$('#responsableResidenteModif').empty();
			$('#listadoResponsablesLabelModif').show();
			$('#listadoResponsablesSelectModif').show();
			$('#habitacionResidenteModif').val(datosResidente.id_habitacion);
			mostrarResponsablesPlanta(datosResidente.id_habitacion);
			$('#responsableResidenteModif').val(datosResidente.id_personal_responsable);
		}else{
			$('#habitacionResidenteModif').val("No tiene habitacion asignada");
			$('#responsableResidenteModif').empty();
			$('#listadoResponsablesLabelModif').hide();
			$('#listadoResponsablesSelectModif').hide();
		}
		
		//recuperamos los datos de DISCAPACIDAD y SITUACION MEDICA
		var respuestaDiscapacidad = $.parseJSON(datosResidente.discapacidad);
		var respuestaSitMedica = $.parseJSON(datosResidente.situacion_medica);
		
		
		
		$discapacidadCheckBoxes.each(function(index, check ){		
			$(check).prop('checked',respuestaDiscapacidad[check.id]);
			if(check.id == "otraDiscapacidad"){
				if(respuestaDiscapacidad[check.id]!=false){
					$("[name='otraDiscapacidadTextoModif']").val(respuestaDiscapacidad['otraDiscapacidadTexto']);
					$("#otraDiscapacidadTextoModif").show();
				}else{
					$("#otraDiscapacidadTextoModif").hide();
					$("[name='otraDiscapacidadTextoModif']").val("");
				}
			}
		});
		
		$sitMedicaCheckBoxes.each(function(index, check ){		
			$(check).prop('checked',respuestaSitMedica[check.id]);
			if(check.id == "otraSitMedica"){
				if(respuestaSitMedica[check.id]!=false){
					$("[name='otraSitMedicaTextoModif']").val(respuestaSitMedica['otraSitMedicaTexto']);
					$("#otraSitMedicaTextoModif").show();
				}else{
					$("#otraSitMedicaTextoModif").hide();
					$("[name='otraSitMedicaTextoModif']").val("");
				}
			}
		});
		
		//calculamos la edad
		$form.find($('[name="edad"]')).val(calcularEdad(datosResidente.fecha_nacimiento));
		
	}else{
		$form.find($('.form-control')).each(function(index){
			$(this).val("");
		});	
		$discapacidadCheckBoxes.each(function(index, check ){		
			$(check).prop('checked',false);
		});
		$sitMedicaCheckBoxes.each(function(index, check ){		
			$(check).prop('checked',false);
		});
		$('#habitacionResidenteModif').val("");
		$('#responsableResidenteModif').empty();
		$('#gradoDependenciaModif').val("0");
		
	}

}


function  busquedaIdResidente(modobusqueda,seccionMenu){

	let idResidente;
		
	if(modobusqueda=="busquedaDni")
		idResidente = $(seccionMenu).find($('.dni-residente')).val();
	else if(modobusqueda=="busquedaApellidos")
		idResidente = $(seccionMenu).find($('.apellidos-residente')).val();
	else if(modobusqueda=="busquedaExpediente")
		idResidente = $(seccionMenu).find($('.expediente-residente')).val();
	else
		idResidente = $(seccionMenu).find($('.habitacion-residente')).val();
		
	return idResidente;
}

function modificarResidente(){
	
	let modoBusqueda = $('#ver-modificar-residente').find('.modoBusquedaResidente').val();
	let idResidente = busquedaIdResidente(modoBusqueda,$('#ver-modificar-residente'));
	let respuesta;
	let datosDiscapacidad=new Object();
	let datosSitMedica=new Object();
	
	let $discapacidadCheckBoxes = $("[class~='discapacidadModif'] input[type=checkbox]");
	let $sitMedicaCheckBoxes = $("[class~='sitMedicaModif'] input[type=checkbox]");
	
	$discapacidadCheckBoxes.each(function(index, check ){
		datosDiscapacidad[check.id] = check.checked;
		if(check.id == "otraDiscapacidad"){
			if(check.checked)
				datosDiscapacidad['otraDiscapacidadTexto'] = $("[name='otraDiscapacidadTextoModif']").val();	
			else
				datosDiscapacidad['otraDiscapacidadTexto']="";
		}
	});
	
	$sitMedicaCheckBoxes.each(function(index, check ){
		datosSitMedica[check.id] = check.checked;
		if(check.id == "otraSitMedica"){
			if(check.checked)
				datosSitMedica['otraSitMedicaTexto'] = $("[name='otraSitMedicaTextoModif']").val();	
			else
				datosSitMedica['otraSitMedicaTexto']="";
		}
	});
			
	var datosResidente ={
		id_residente : idResidente,
        dni_residente : $('#dniResidenteModif').val(),
        nombre : $('#nombreResidenteModif').val(),
        apellido1 : $('#apellido1ResidenteModif').val(),
        apellido2 : $('#apellido2ResidenteModif').val(),
        fecha_nacimiento: $('#fechaNacimientoModif').val(),
        grado_dependencia : $('#gradoDependenciaModif').val(),
		discapacidad : JSON.stringify(datosDiscapacidad,escaparComillas),
		situacion_medica : JSON.stringify(datosSitMedica,escaparComillas),
		id_personal_responsable : $('#responsableResidenteModif').val()
	};
	
	var datosFamiliar={
		
		dni_familiar : $('#dniFamiliarModif').val(),
        nombre_familiar : $('#nombreFamiliarModif').val(),
        apellidos : $('#apellidosFamiliarModif').val(),
        direccion_postal : $('#direccionFamiliarModif').val(),
		codigo_postal : $('#codigoPostalFamiliarModif').val(),
		parentesco : $('#parentescoFamiliarModif').val(),
        telefono : $('#telefonoFamiliarModif').val(),
        email : $('#emailFamiliarModif').val()
	};
				
	var objetoCamposWhere = {			
		id_residente : idResidente		
	}
	
	$.ajax({
		url: '../controllers/residente.php?task=modificarResidente',
		type: "POST",
		async: false,
		data: {datosResidente,datosFamiliar,objetoCamposWhere},
		dataType: "html",
		success: function(data) {
			let respuesta =  $.parseJSON(data);
			if(respuesta.status == "WARNING"){
				swal({
					title: "MODIFICADO CON WARNING",
					text:  respuesta.message,
					buttons: false,
					icon:  "warning",
					timer: 2500,
				});
				
			}else{
				swal({
					title: "MODIFICADO",
					text:  "Residente modificado correctamente",
					buttons: false,
					icon:  "success",
					timer: 1500,
				});
			}
			setTimeout('location.reload()',2000); 
		},
		error: function(data) {
			let respuesta =  $.parseJSON(data.responseText);
			swal({
				title: "ERROR",
				text:  respuesta.message,
				buttons: false,
				icon:  "error",
				timer: 2500,
			});
		}
	});
}

function cargarDatosContacto(){
	
	let modoBusqueda = $('#ver-datos-contacto').find('.modoBusquedaResidente').val();
	let idResidente = busquedaIdResidente(modoBusqueda,$('#ver-datos-contacto'));
	
	if(idResidente != undefined && idResidente != ""){
		$('#ver-tabla-datos').show(); //tabla-horario
		tablaDatosContacto(idResidente);		
	}else
		$('#ver-tabla-datos').hide();	
}

function calcularEdad(fechaNacimiento){
	let hoy = new Date();
	let fechaArray= fechaNacimiento.split("/");
    let cumpleanos = new Date(fechaArray[2],fechaArray[1]-1,fechaArray[0]);
    let edad = hoy.getFullYear() - cumpleanos.getFullYear();
    let mes = hoy.getMonth() - cumpleanos.getMonth();

    if(edad <= 0)
		edad = 0;
	else if (mes < 0 || (mes == 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }
    return edad;	
}

function escaparComillas(key, value) {
	let resultado = value;
  if (typeof value === "string") {
	resultado = value.replace(/\"/g, '\\"');
  }
  return resultado;
}

function activarResidente(idResidente){

	var objetoCamposUpdate = {			
		fecha_baja : "null",
		motivo_baja : "null"
	}
	
	var objetoCamposWhere = {			
		id_residente : idResidente
	}

    $.ajax({
        url: '../controllers/residente.php?task=activarResidente',
        type: "POST",
        async: false,
        data: {objetoCamposUpdate,objetoCamposWhere},
		dataType: "html",
        success: function(data) {
			swal({
				title: "ACTUALIZADO",
				text:  "Residente activado correctamente",
				buttons: false,
				icon:  "success",
				timer: 2500,
			});
		},
		error: function(data) {
			swal({
				title: "ERROR",
				text:  "No ha sido posible realizar la activación",
				buttons: false,
				icon:  "error",
				timer: 2500,
			});
		}
	});
	
	
}

//FUNCION QUE GENERA TABLA CON DATOS DE CONTACTO DEL RESIDENTE
var tablaDatosContacto = function(id){
    var table = $('#tabla-datos').DataTable({
       dom: '', //dom: 'Bfrtip',
       buttons: [
             /*'excel', 'pdf', 'print'*/
       ],
        "destroy":true, //para que la tabla la pueda volver a cargar en cada consulta
        "ajax":{
			"method":"POST",
			"url":"../controllers/tablas.php?funcion=datoscontacto&idResidente="+id+""
        },
        "columns":[
            {"data":"nombre_familiar"},
            {"data":"apellidos"},
            {"data":"direccion_postal"},
			{"data":"codigo_postal"},
            {"data":"telefono"},
            {"data":"email"},
			{"data":"parentesco"}
        ]
    });
}

//FUNCION QUE GENERA TABLA DE LOS RESIDENTES HISTORICOS (NO ACTIVOS)
var tablaResidenteHistorico = function(){
    var table = $('#tabla-residente-historico').DataTable({
        dom: 'Bfrtip',
        buttons:[
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o" style="font-size: 1.25rem"></i> ',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o" style="font-size: 1.25rem"></i> ',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger',
                pageSize: 'A3'
            },
            {
                extend:    'print',
                text:      '<i class="fa fa-print" style="font-size: 1.25rem"></i> ',
				titleAttr: 'Imprimir',
                className: 'btn btn-info'
            }
        ],
        "order": [ 0, 'desc' ],
        "destroy":true,
        "ajax":{
          "method":"POST",
          "url":"../controllers/tablas.php?funcion=residentehistorico"
        },
        "columns":[
			{"data":"fecha_alta",visible: false},
			{"data":"n_expediente"},
			{"data":"dni_residente"},
			{"data":"nombre"},
			{"data":"apellido1"},
			{"data":"apellido2"},
			{"data": "fecha_nacimiento",render: function(fecha){
				return calcularEdad(fecha);
				}
			},
			{"data":"discapacidad"},
			{"data":"situacion_medica"},
			{"data":"grado_dependencia"},
			{"data":"fecha_alta", "orderData": 0, render: function(date){
				return dateFns.format(date, 'DD/MM/YYYY HH:mm')
				}
			},
			{"data":"fecha_baja", render: function(date){
				return dateFns.format(date, 'DD/MM/YYYY HH:mm')
				}
			},
			{"data":"motivo_baja"},
			{"data":"id_residente", render: function(idResidente){
				return '<div style="text-align: center;" ><input class="btn btn-outline-primary botonActivar botonActivarResidente" type="button" value="Activar" id='+idResidente+'></div>'
				}
			}
        ]
    });
	table.on( 'draw', function () {
		   $('.botonActivarResidente').click(function(){
				swal({
					title: "CAMBIAR A RESIDENTE EN ACTIVO",
					text:  "¿Está seguro de que quiere cambiar el estado a ACTIVO?",
					buttons: {
						Cancelar: {text: "Cancelar"},
						Confirmar: {text: "Confirmar"},
					},
					icon:  "warning"
				})
				.then((value) => {
					switch (value) {
						case "Confirmar":
							activarResidente($(this).attr("id"));
							tablaResidenteHistorico();
							if($('#de-alta-residente').is(':checked'))
								tablaResidenteDeAlta();
							setTimeout('location.reload()',2000);
							break;
						case "Cancelar":
							break;
					}
				});
			});
	   });
}

//FUNCION QUE GENERA TABLA CON TODOS LOS RESIDENTES ACTIVOS ACTUALES
    var tablaResidenteDeAlta = function(){
      var table = $('#tabla-residente-de-alta').DataTable({
          dom: 'Bfrtip',
          buttons:[
            {
                extend:    'excelHtml5',
                text:      '<i class="fa fa-file-excel-o" style="font-size: 1.25rem"></i> ',
                titleAttr: 'Exportar a Excel',
                className: 'btn btn-success'
            },
            {
                extend:    'pdfHtml5',
                text:      '<i class="fa fa-file-pdf-o" style="font-size: 1.25rem"></i> ',
                titleAttr: 'Exportar a PDF',
                className: 'btn btn-danger',
                pageSize: 'A3'
            },
            {
                extend:    'print',
                text:      '<i class="fa fa-print" style="font-size: 1.25rem"></i> ',
                titleAttr: 'Imprimir',
                className: 'btn btn-info'
            }
        ],
        "order": [ 0, 'desc' ],
        "destroy":true,
        "ajax":{
            "method":"POST",
            "url":"../controllers/tablas.php?funcion=residentealta"
        },
        "columns":[
			{"data":"fecha_alta",visible: false},
            {"data":"n_expediente"},
			{"data":"dni_residente"},
			{"data":"nombre"},
			{"data":"apellido1"},
			{"data":"apellido2"},
			{"data": "fecha_nacimiento",render: function(fecha){
				return calcularEdad(fecha);
				}
			},
			{"data":"discapacidad"},
			{"data":"situacion_medica"},
			{"data":"grado_dependencia"},
			{"data":"fecha_alta", "orderData": 0, render: function(date){
				return dateFns.format(date, 'DD/MM/YYYY HH:mm')
				}
			}
        ]
    });
 }


