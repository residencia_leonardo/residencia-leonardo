<!-- ALTA DE RESIDENTE -->
<div id="ver-alta-residente" class="seccion-menu" style="display: none;">
	<div class=" align-items-center justify-content-between text-center mb-5">
		<h1 class="h3 mb-0 text-gray-800">Alta Residente</h1>
	</div>

	<div class="row">
		<div class="col-12 mb-4 text-center titulo-seccion">Datos del Residente:</div>
	</div>

	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">DNI:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			<div class="input-group form-group">
				<div class="input-group-prepend ">
					<span class="input-group-text"><i class="fa fa-id-card-o"></i></span>
				</div>
				<input type="text" name="dniResidente" class="form-control" id="dniResidente" placeholder="">
			</div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Nombre:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			<div class="input-group form-group">
				<input type="text" name="nombreResidente" class="form-control" id="nombreResidente" placeholder="">
			</div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
	</div>

	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Primer Apellido:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			<div class="input-group form-group">
				<input type="text" name="apellido1Residente" class="form-control" id="apellido1Residente" placeholder="">
			</div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Segundo Apellido:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			<div class="input-group form-group">
				<input type="text" name="apellido2Residente" class="form-control" id="apellido2Residente" placeholder="">
			</div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
	</div>

	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Fecha de Nacimiento:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			<div class="input-group form-group">
				<input type="text" class="form-control datepicker" name="fechaNacimiento" id="fechaNacimiento" placeholder="Seleccionar Fecha">
			</div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Grado de Dependecia:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			<div class="input-group mb-3">
				<select class="custom-select" id="gradoDependencia">
					<option value="0">Seleccione Grado de Dependencia:</option>
					<option value="1">Grado I: Dependencia Moderada</option>
					<option value="2">Grado II: Dependencia Severa</option>
					<option value="3">Grado III: Gran Dependecia</option>
				</select>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Edad:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			<div class="input-group form-group">
				<input type="number" name="edad" class="form-control" id="edadResidente" disabled>
			</div>
		</div>
	</div>
	
	<div class="row mt-2">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Asignar Habitación:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			<div class="row mt-1">
				<div class="col-12 text-center">
					<div class="form-check form-check-inline">
						<input class="form-check-input habIndividual" type="radio" name="tipoHabitacion" value="INDIVIDUAL">
						<label class="form-check-label" for="habIndividual">Individual</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input habDoble" type="radio" name="tipoHabitacion" value="DOBLE">
						<label class="form-check-label" for="habDoble">Doble</label>
					</div>
				</div>
			</div>
			<div class="row mt-1 habitacionIndividual" style="display: none;">
				<div class="col-12 text-center">
					<div class="input-group mb-3 ">
						<select class="custom-select form-control-sm" id="habitacionIndivAsignada" >
							<option value="">Seleccione Habitación</option>
							<?php
							foreach($habitacioneslibresIndividuales as $habitacion){
							?>
								<option value="<?php echo $habitacion[0] ?>"><?php echo $habitacion[0] ?></option>
							<?php 
							}
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="row mt-1 habitacionDoble" style="display: none;">
				<div class="col-12 text-center">
					<div class="input-group mb-3 ">
						<select class="custom-select form-control-sm" id="habitacionDobleAsignada">
							<option value="">Seleccione Habitación</option>
							<?php
							foreach($habitacioneslibresDobles as $habitacion){
							?>
								<option value="<?php echo $habitacion[0] ?>"><?php echo $habitacion[0] ?></option>
							<?php 
							}
							?>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="mt-4 col-xl-2 col-lg-2 col-md-2 col-sm-2" id="listadoResponsablesLabel" style="padding-top: 5px; display: none;">
			<label class="font-weight-bold text-gray-800">Profesional Responsable:</label>
		</div>
		<div class="mt-4 col-xl-3 col-lg-3 col-md-10 col-sm-10" id="listadoResponsablesSelect" style="padding-top: 5px; display: none;">
			<div class="input-group mb-3">
				<select class="custom-select responsableResidente" id="responsableResidente">
					<option value="">Seleccione Responsable</option>
				</select>
			</div>
		</div>
	</div>
	
	<div class="row mt-4">
		<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 ">
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 " style="padding-top: 5px;">
					<label class="font-weight-bold text-gray-800">Discapacidad:</label>
				</div>
			</div>
			<div class="row  checkbox-row discapacidadAlta">
				<div class="col-10 form-check">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="intelectual">
						<label class="form-check-label mr-1" for="intelectual">Intelectual</label>
					</div>
					<div class=" col-xl-3 col-lg-3 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="fisica">
						<label class="form-check-label mr-1" for="fisica">Física</label>
					</div>
					<div class=" col-xl-3 col-lg-3  col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="visual">
						<label class="form-check-label" for="visual">Visual</label>
					</div>
				</div>
			</div>
			<div class="row  checkbox-row discapacidadAlta">
				<div class=" col-10 form-check">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input mr-1" id="enfermendadMental">
						<label class="form-check-label" for="enfermendadMental">Enf. Mental</label>
					</div>
					<div class=" col-xl-3 col-lg-3 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input mr-1" id="demencia">
						<label class="form-check-label mr-1" for="demencia">Demencia</label>
					</div>
					<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="otraDiscapacidad">
						<label class="form-check-label" for="otraDiscapacidad">Otra</label>					
					</div>
				</div>
			</div>
			<div class="row mt-2"  id="otraDiscapacidadTexto" style="display: none;">
				<div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
					<div class="input-group form-group">
						<input type="text" name="otraDiscapacidadTexto" class="form-control form-control-sm" placeholder="Introduzca Discapacidad">
					</div>
				</div>
			</div>
		</div>

		
	</div>
	
	<div class="row mt-4">
		<div class="col-12" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Situación Médica:</label>
		</div>
		<div class="col-12 form-check">
			<div class="row  checkbox-row sitMedicaAlta">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="enfermedadLeve">
						<label class="form-check-label" for="enfermedadLeve">Enfermedad leve</label>
					</div>
					<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="pronosticoReservado">
						<label class="form-check-label mr-1" for="pronosticoReservado">Pronóstico reservado (en espera de resultados)</label>
					</div>
					
				</div>
			</div>
			<div class="row  checkbox-row sitMedicaAlta">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="tratamCurativo">
						<label class="form-check-label mr-1" for="tratamCurativo">Tratamiento curativo</label>
					</div>
					<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="tratamPaliativo">
						<label class="form-check-label" for="tratamPaliativo">Tratamiento paliativo</label>
					</div>	
				</div>
			</div>
			<div class="row  checkbox-row sitMedicaAlta">
				<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input mr-1" id="tratamFarmaTemp">
						<label class="form-check-label" for="tratamFarmaTemp">Tratamiento farmacológico temporal</label>
					</div>
					<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input mr-1" id="tratamFarmaCron">
						<label class="form-check-label mr-1" for="tratamFarmaCron">Tratamiento farmacológico crónico</label>
					</div>				
				</div>
			</div>
			<div class="row  checkbox-row sitMedicaAlta">
				<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="deterioroCognitivo">
						<label class="form-check-label" for="deterioroCognitivo">Deterioro cognitivo</label>
					</div>
					<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="debilidadDebMotriz">
						<label class="form-check-label" for="debilidadDebMotriz">Debilidad motriz</label>
					</div>				
				</div>
			</div>
			<div class="row  checkbox-row sitMedicaAlta">
				<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="desorientacion">
						<label class="form-check-label" for="desorientacion">Desorientación</label>
					</div>
					<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="DificultadLenguaje">
						<label class="form-check-label" for="DificultadLenguaje">Dificultades del Lenguaje</label>
					</div>				
				</div>
			</div>
			<div class="row  checkbox-row sitMedicaAlta">
				<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class="col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
						<input type="checkbox" class="form-check-input" id="otraSitMedica">
						<label class="form-check-label" for="otraSitMedica">Otra</label>					
					</div>
				</div>
			</div>
			<div class="row mt-2" id="otraSitMedicaTexto" style="display: none;">
				<div class=" col-xl-5 col-lg-5 col-md-12 col-sm-12 ">
					<div class="input-group form-group">
							<input type="text" name="otraSitMedicaTexto" class="form-control form-control-sm" placeholder="Introduzca Situación Médica">
					</div>
				</div>
			</div>
		</div>

	</div>
	

	<div class="row mt-2">
		<div class="col-12 mb-4 mt-4 text-center titulo-seccion">Datos Persona de Contacto:</div>
	</div>

	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			<label class="font-weight-bold text-gray-800">Parentesco:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
		  <div class="input-group form-group">
			<input type="text" name="parentesco" class="form-control" id="parentescoFamiliar" placeholder="">
		  </div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">DNI:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
		  <div class="input-group form-group">
			<div class="input-group-prepend ">
			  <span class="input-group-text"><i class="fa fa-id-card-o"></i></span>
			</div>
			<input type="text" name="dni" class="form-control" id="dniFamiliar" placeholder="">
		  </div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">Nombre:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
		  <div class="input-group form-group">
			<input type="text" name="nombre" class="form-control" id="nombreFamiliar" placeholder="">
		  </div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">Apellidos:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
		  <div class="input-group form-group">
			<input type="text" name="apellidos" class="form-control" id="apellidosFamiliar" placeholder="">
		  </div>
		</div>
	</div>
  
	<div class="row">
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">Dirección Postal:</label>
		</div>
		<div class="col-xl-3 col-lg-9 col-md-10 col-sm-10" >
		  <div class="input-group form-group">
			<input type="text" name="direccion" class="form-control" id="direccionFamiliar" placeholder="">
		  </div>
		</div>
		<div class="col-xl-1 col-lg-0 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">Codigo Postal:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
		  <div class="input-group form-group">
			<input type="number"  min="1000" max="52999" name="codigoPostal" class="form-control" id="codigoPostalFamiliar" placeholder="">
		  </div>
		</div>
		<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">Telefono:</label>
		</div>
		<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
		  <div class="input-group form-group">
			<input type="text" name="telefono" class="form-control" id="telefonoFamiliar" placeholder="">
		  </div>
		</div>
		<div class="col-xl-1 col-lg-0 col-md-0 col-sm-0"></div>
		<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
		  <label class="font-weight-bold text-gray-800">Email:</label>
		</div>
		<div class="col-xl-3 col-lg-9 col-md-10 col-sm-10" >
		  <div class="input-group form-group">
			<input type="email" name="email" class="form-control" id="emailFamiliar" placeholder="">
		  </div>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
			<button type="button" class="btn btn-success" id="boton-alta-residente">Alta Residente</button>
		</div>
	</div>

</div>