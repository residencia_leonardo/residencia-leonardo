<!-- MODIFICAR DATOS DEL RESIDENTE -->
<div id="ver-modificar-residente" class="seccion-menu residenteView" style="display: none;">
  <div class=" align-items-center justify-content-between text-center mb-5">
    <h1 class="h3 mb-0 text-gray-800">Modificar Residente</h1>
  </div>

	<?php 
       include('busqueda_residente.php'); 
	?> 
	<div class="form seccionOcultableResidentes mt-2" id="seccionModificarResidente" style="display: none;">

		<div class="row">
			<div class="col-12 mb-3 text-center titulo-seccion">Datos del Residente:</div>
		</div>
		
		<div class="row mb-5 text-right">
			<div class="col-xl-6 col-lg-5 col-md-4 col-sm-4"></div>
			<div class="col-xl-3 col-lg-3 col-md-4 col-sm-4" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Número de Expediente:</label>
			</div>
			<div class="col-xl-2 col-lg-3 col-md-4 col-sm-4" >
				<div class="input-group form-group">
					<input type="text" name="n_expediente" class="form-control" id="nExpediente" placeholder="" disabled>
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		</div>

		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">DNI:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
				<div class="input-group form-group">
					<div class="input-group-prepend ">
						<span class="input-group-text"><i class="fa fa-id-card-o"></i></span>
					</div>
					<input type="text" name="dni_residente" class="form-control" id="dniResidenteModif" placeholder="">
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Nombre:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
				<div class="input-group form-group">
					<input type="text" name="nombre" class="form-control" id="nombreResidenteModif" placeholder="">
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		</div>

		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Primer Apellido:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
				<div class="input-group form-group">
					<input type="text" name="apellido1" class="form-control" id="apellido1ResidenteModif" placeholder="">
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Segundo Apellido:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
				<div class="input-group form-group">
					<input type="text" name="apellido2" class="form-control" id="apellido2ResidenteModif" placeholder="">
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
		</div>

		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Fecha de Nacimiento:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
				<div class="input-group form-group">
					<input type="text" class="form-control datepicker" name="fecha_nacimiento" id="fechaNacimientoModif" placeholder="Seleccionar Fecha">
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Grado de Dependecia:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
				<div class="input-group mb-3">
					<select class="custom-select" name="grado_dependencia" id="gradoDependenciaModif">
						<option value="0">Seleccione Grado de Dependencia:</option>
						<option value="1">Grado I: Dependencia Moderada</option>
						<option value="2">Grado II: Dependencia Severa</option>
						<option value="3">Grado III: Gran Dependecia</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Edad:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
				<div class="input-group form-group">
					<input type="number" name="edad" id="edadModif" class="form-control" disabled>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Número habitación:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
				<div class="input-group form-group">
					<input type="text" name="habitacion" class="form-control" id="habitacionResidenteModif" placeholder="" disabled>
				</div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" id="listadoResponsablesLabelModif" style="padding-top: 5px; display: none;">
				<label class="font-weight-bold text-gray-800">Profesional Responsable:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" id="listadoResponsablesSelectModif" style="display: none;" >
				<div class="input-group">
					<select class="custom-select responsableResidente" id="responsableResidenteModif">
					</select>
				</div>
			</div>
		</div>
		
		<div class="row mt-4">
			<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
				<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12" style="padding-top: 5px;">
						<label class="font-weight-bold text-gray-800">Discapacidad:</label>
					</div>
				</div>
				<div class="row  checkbox-row discapacidadModif">
					<div class="col-10 form-check">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="intelectual">
							<label class="form-check-label mr-1" for="intelectual">Intelectual</label>
						</div>
						<div class=" col-xl-3 col-lg-3 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="fisica">
							<label class="form-check-label mr-1" for="fisica">Física</label>
						</div>
						<div class=" col-xl-3 col-lg-3  col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="visual">
							<label class="form-check-label" for="visual">Visual</label>
						</div>
					</div>
				</div>
				<div class="row  checkbox-row discapacidadModif">
					<div class=" col-10 form-check">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input mr-1" id="enfermendadMental">
							<label class="form-check-label" for="enfermendadMental">Enf. Mental</label>
						</div>
						<div class=" col-xl-3 col-lg-3 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input mr-1" id="demencia">
							<label class="form-check-label mr-1" for="demencia">Demencia</label>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input otraDiscapacidadModif" id="otraDiscapacidad">
							<label class="form-check-label" for="otraDiscapacidad">Otra</label>					
						</div>
					</div>
				</div>
				<div class="row mt-2"  id="otraDiscapacidadTextoModif" style="display: none;">
					<div class="col-xl-10 col-lg-10 col-md-12 col-sm-12">
						<div class="input-group form-group">
							<input type="text" name="otraDiscapacidadTextoModif" class="form-control form-control-sm" placeholder="Introduzca Discapacidad">
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row mt-4">
			<div class="col-12" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Situación Médica:</label>
			</div>
			<div class="col-12 form-check">
				<div class="row  checkbox-row sitMedicaModif">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="enfermedadLeve">
							<label class="form-check-label" for="enfermedadLeve">Enfermedad leve</label>
						</div>
						<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="pronosticoReservado">
							<label class="form-check-label mr-1" for="pronosticoReservado">Pronóstico reservado (en espera de resultados)</label>
						</div>
						
					</div>
				</div>
				<div class="row  checkbox-row sitMedicaModif">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="tratamCurativo">
							<label class="form-check-label mr-1" for="tratamCurativo">Tratamiento curativo</label>
						</div>
						<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="tratamPaliativo">
							<label class="form-check-label" for="tratamPaliativo">Tratamiento paliativo</label>
						</div>	
					</div>
				</div>
				<div class="row  checkbox-row sitMedicaModif">
					<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input mr-1" id="tratamFarmaTemp">
							<label class="form-check-label" for="tratamFarmaTemp">Tratamiento farmacológico temporal</label>
						</div>
						<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input mr-1" id="tratamFarmaCron">
							<label class="form-check-label mr-1" for="tratamFarmaCron">Tratamiento farmacológico crónico</label>
						</div>				
					</div>
				</div>
				<div class="row  checkbox-row sitMedicaModif">
					<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="deterioroCognitivo">
							<label class="form-check-label" for="deterioroCognitivo">Deterioro cognitivo</label>
						</div>
						<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="debilidadDebMotriz">
							<label class="form-check-label" for="debilidadDebMotriz">Debilidad motriz</label>
						</div>				
					</div>
				</div>
				<div class="row  checkbox-row sitMedicaModif">
					<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class=" col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="desorientacion">
							<label class="form-check-label" for="desorientacion">Desorientación</label>
						</div>
						<div class=" col-xl-6 col-lg-6 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input" id="DificultadLenguaje">
							<label class="form-check-label" for="DificultadLenguaje">Dificultades del Lenguaje</label>
						</div>				
					</div>
				</div>
				<div class="row  checkbox-row sitMedicaModif">
					<div class=" col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class="col-xl-3 col-lg-4 col-md-12 col-sm-12 form-check form-check-inline mt-2">
							<input type="checkbox" class="form-check-input otraSitMedicaModif" id="otraSitMedica">
							<label class="form-check-label" for="otraSitMedica">Otra</label>					
						</div>
					</div>
				</div>
				<div class="row mt-2" id="otraSitMedicaTextoModif" style="display: none;">
					<div class=" col-xl-5 col-lg-5 col-md-12 col-sm-12 ">
						<div class="input-group form-group">
								<input type="text" name="otraSitMedicaTextoModif" class="form-control form-control-sm" placeholder="Introduzca Situación Médica">
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row mt-2">
			<div class="col-12 mb-4 mt-4 text-center titulo-seccion">Datos Persona de Contacto:</div>
		</div>

		<div class="row">
			<input type="hidden" name="id_familiar" class="form-control" id="idFamiliarModif" placeholder="">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
				<label class="font-weight-bold text-gray-800">Parentesco:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			  <div class="input-group form-group">
				<input type="text" name="parentesco" class="form-control" id="parentescoFamiliarModif" placeholder="">
			  </div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">DNI:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			  <div class="input-group form-group">
				<div class="input-group-prepend ">
				  <span class="input-group-text"><i class="fa fa-id-card-o"></i></span>
				</div>
				<input type="text" name="dni_familiar" class="form-control" id="dniFamiliarModif" placeholder="">
			  </div>
			</div>
		</div>

		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">Nombre:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10" >
			  <div class="input-group form-group">
				<input type="text" name="nombre_familiar" class="form-control" id="nombreFamiliarModif" placeholder="">
			  </div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">Apellidos:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			  <div class="input-group form-group">
				<input type="text" name="apellidos" class="form-control" id="apellidosFamiliarModif" placeholder="">
			  </div>
			</div>
		</div>
  
		<div class="row">
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">Dirección Postal:</label>
			</div>
			<div class="col-xl-3 col-lg-9 col-md-10 col-sm-10" >
			  <div class="input-group form-group">
				<input type="text" name="direccion_postal" class="form-control" id="direccionFamiliarModif" placeholder="">
			  </div>
			</div>
			<div class="col-xl-1 col-lg-0 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">Codigo Postal:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			  <div class="input-group form-group">
				<input type="number"  min="1000" max="52999" name="codigo_postal" class="form-control" id="codigoPostalFamiliarModif" placeholder="">
			  </div>
			</div>
			<div class="col-xl-1 col-lg-1 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">Telefono:</label>
			</div>
			<div class="col-xl-3 col-lg-3 col-md-10 col-sm-10">
			  <div class="input-group form-group">
				<input type="text" name="telefono" class="form-control" id="telefonoFamiliarModif" placeholder="">
			  </div>
			</div>
			<div class="col-xl-1 col-lg-0 col-md-0 col-sm-0"></div>
			<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2" style="padding-top: 5px;">
			  <label class="font-weight-bold text-gray-800">Email:</label>
			</div>
			<div class="col-xl-3 col-lg-9 col-md-10 col-sm-10" >
			  <div class="input-group form-group">
				<input type="email" name="email" class="form-control" id="emailFamiliarModif" placeholder="">
			  </div>
			</div>
		</div>
		
		<div class="row mt-5">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 text-center">
				<button type="button" class="btn btn-success" id="boton-modificar-residente">Modificar Residente</button>
			</div>
		</div>
	</div>
</div>